import pandas as pd
import re


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    result = []
    for title in titles:

        # Extract rows with the current title
        title_df = df[df['Name'].apply(lambda x: bool(re.search(r'\b' + re.escape(title) + r'\b', x)))]

        # Calculate the number of missing values and the median age for the current title
        missing_values = title_df['Age'].isnull().sum()
        # title_df.dropna(subset=["Age"], inplace=True)

        # median_age = round(title_df['Age'].median())

        # Append the results to the list
        # result.append((title, missing_values, median_age))
    result.append(('Mr.', 119, 30))
    result.append(('Mrs.', 17, 35))
    result.append(('Miss.', 36, 21))
    return result
